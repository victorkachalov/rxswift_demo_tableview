//
//  Food.swift
//  RxCocoaTest
//
//  Created by Константин on 23.09.16.
//  Copyright © 2016 Константин. All rights reserved.
//

import UIKit

struct Food {
    let name: String
    let flickrID: String
    var image: UIImage?
    
    init(name: String, flickrID: String) {
        self.name = name
        self.flickrID = flickrID
        image = UIImage(named: flickrID)
    }

}

extension Food: CustomStringConvertible {
    var description: String {
        return "\(name): flickr.com/\(flickrID)"
    }
}
